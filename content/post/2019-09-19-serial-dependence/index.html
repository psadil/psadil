---
title: Serial Dependence
author: Patrick Sadil
date: '2019-09-19'
slug: serial-dependence
categories:
  - dissertation
tags: []
image:
  caption: ''
  focal_point: ''
link-citations: true
bibliography: [one.bib]
---

<link href="index_files/anchor-sections/anchor-sections.css" rel="stylesheet" />
<script src="index_files/anchor-sections/anchor-sections.js"></script>


<p>Objects in the visual environment move suddenly and erratically, and visual perception must be sensitive to the changes that are important. But each saccade and head tilt change the image imprinted on the retina, and to perceive every tremor ignores the stability of the visual environment; a desk will still look like a desk in a few seconds. The visual system must therefore balance the ability to detect subtle changes in the environment against the efficiency afforded by accurate predictions.</p>
<p>That the recent past influences current perception can be demonstrated easily. If you stare at Figure <a href="#fig:tae"><strong>??</strong></a>, you might observe that the Gabor has a bend immediately after changing orientations. The bend lasts for a moment, then straightens. But the bend is an illusion. While tracking Figure <a href="#fig:tae"><strong>??</strong></a>, the visual system allows for a momentary bias. Usefully, the bias is sensitive to experimental manipulation. Figure <a href="#fig:tae2"><strong>??</strong></a> shows the same Gabor with the same orientations, but the Gabor also moves. The movement largely eliminates the bending. The sensitivity of such biases to different experimental manipulations enables researchers to study how the visual system balances new information against the recent past.</p>
<p>A closely effect is called <em>serial dependence</em>. Serial dependence occurs when participants report the orientations of sequentially presented, tilted Gabors <span class="citation">(Fischer and Whitney <a href="#ref-fischer2014" role="doc-biblioref">2014</a>)</span>. A visual mask to reduces the strong aftereffects present in Figures <a href="#fig:tae"><strong>??</strong></a> and <a href="#fig:tae2"><strong>??</strong></a> [Figure <a href="#fig:gabor"><strong>??</strong></a>]<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>. Even without the aftereffect, the perceptual-decision about one Gabor affects the perceptual-decision about the next; participants report orientations that consistently err toward the orientation of the most recently seen Gabor. Reports on the magnitude of the effect vary, but the error has an average maximum of less than a few degrees. However, serial dependence is affected by different manipulations than that the demonstration of Figures <a href="#fig:tae"><strong>??</strong></a> and <a href="#fig:tae2"><strong>??</strong></a>. For example, it appears insensitive to the location of the Gabors. This bias may therefore provide a unique way to study how current perception is not only biased by but toward the recent past.</p>
<p>However, it remains unclear whether serial dependence is a bias of perceptual or post-perceptual processes. That is, does serial dependence alter participants’ perception of the Gabors, or does it alter how they report the orientation? The sequential timing of each trial – in which participants respond in a designated period after seeing the Gabor – does not imply that participants decide on an orientation only after they have finished perceiving the Gabor. For example, a participant can make decisions before the response period, and they can adopt a biased response strategy even before seeing the Gabor. Where and when to delineate between perception and decision, or whether they can be delineated, depends on assumptions about the relationship between perception and decisions. A tool like the <a href="https://psadil.gitlab.io/psadil/post/circular-diffusion-model-of-response-times/">circular diffusion model</a> can help make those assumptions explicit <span class="citation">(Smith <a href="#ref-smith2016" role="doc-biblioref">2016</a>)</span>.</p>
<div id="references" class="section level1 unnumbered">
<h1>References</h1>
<div id="refs" class="references">
<div id="ref-fischer2014">
<p>Fischer, Jason, and David Whitney. 2014. “Serial Dependence in Visual Perception.” <em>Nature Neuroscience</em> 17 (5): 738.</p>
</div>
<div id="ref-smith2016">
<p>Smith, Philip L. 2016. “Diffusion Theory of Decision Making in Continuous Report.” <em>Psychological Review</em> 123 (4): 425–51. <a href="https://doi.org/10.1037/rev0000023">https://doi.org/10.1037/rev0000023</a>.</p>
</div>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>The timing and spacing of this figure does not quite match a typical experiment. For example, participants take a few seconds to respond, so the amount of time between Gabors in this figure is too short.<a href="#fnref1" class="footnote-back">↩︎</a></p></li>
</ol>
</div>
