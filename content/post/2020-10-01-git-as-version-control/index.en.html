---
title: Git as version control
author: Patrick Sadil
date: '2020-10-01'
slug: []
categories: []
tags: []
draft: true
---

<script src="{{< blogdown/postref >}}index.en_files/header-attrs/header-attrs.js"></script>


<p>This is a first in a series of posts that will attempt to explain</p>
<div id="the-problem-to-solve" class="section level1">
<h1>The problem to solve</h1>
<p>Jokes, <a href="https://xkcd.com/1459/">1</a> and <a href="http://phdcomics.com/comics/archive.php?comicid=1531">2</a>.</p>
<p>With many systems of digital memory, the problem is rarely space; we can store loads of data indefinitely. Instead, the challenge is to develop a good system of indexing things, so that you can find what you’ve backed up. For this, it would be helpful to know about which commits are the most important.</p>
<p>Git can be a system for “version control”. I don’t have much nuance to add to this phrase, other than to highlight that, in the long run, it will be helpful to spend time thinking about what is meant by a “version” for research. For myself, I find that a “version” means a particular output: each pilot of an experiment, the code right before a meeting, all of your analysis right before a poster, the analyses immediately before a publication. I find that a version tends to mean the exact state of a project as a certain time, something that I want to be easily accessible to a future me. Your own definition of a version will probably change as you go through graduate school.</p>
</div>
<div id="assumptions" class="section level1">
<h1>Assumptions</h1>
<ul>
<li>You have installed <code>Git</code>.</li>
<li>You know how to access a terminal that knows about <code>Git</code></li>
<li>You’ve done the initial configuration steps</li>
</ul>
<div class="marginnote">
<p><a href="https://git-scm.com/book/en/v2/Getting-Started-Installing-Git">Git Pro Book</a></p>
</div>
</div>
<div id="initialize-a-repository-with-git-init" class="section level1">
<h1>Initialize a repository with <code>git init</code></h1>
<p>Make a new directory. Call it “demo”. Start <code>Git</code> in that folder. The first step will be to ‘initialize’ <code>Git</code>. Here, to initialize is the verb used for associating <code>Git</code> with a particular folder.</p>
<pre class="bash"><code>$ git init</code></pre>
<p>New terminology! A “repository”, often shortened to “repo” is a particular folder that is under version control by <code>Git</code>.</p>
</div>
<div id="check-new-status-with-git-status" class="section level1">
<h1>Check new status with <code>git status</code></h1>
<p>There are many situations in which you will want to understand what’s new with a repository. The subcommand for this is <code>staus</code></p>
<pre class="bash"><code>$ git status</code></pre>
<p>Three new sets of words: “branch” (with “master”), “commit”, and “track”. Starting with the last of these, to track is the verb <code>Git</code> uses to mean something whose version is controlled. So, this tip is saying that the next step of version control will involve directing <code>Git</code> which files it should look for.</p>
</div>
<div id="make-changes-to-the-repository" class="section level1">
<h1>Make changes to the repository</h1>
<p>We’ll work with a few simple scripts from <a href="https://fishsciences.github.io/post/visualizing-fish-encounter-histories/" class="uri">https://fishsciences.github.io/post/visualizing-fish-encounter-histories/</a>.</p>
<p>Add the file <code>fishdata.csv</code> to your folder, and take a look at how <code>Git</code> has responded.</p>
<pre class="bash"><code>$ git status</code></pre>
<p>Again, we see the same messages about branches and commits. Now we, also see a message about an “untracked” file. This is a file that is in the repository, but which is not yet under version control.</p>
</div>
<div id="track-files-with-git-add" class="section level1">
<h1>Track files with <code>git add</code></h1>
<p>You must explicitly instruct <code>Git</code> to track files. To track the <code>fishdata.csv</code> file you can run the following</p>
<pre class="bash"><code>$ git add fishdata.csv</code></pre>
<p>Check out the new status</p>
<pre class="bash"><code>$ git status</code></pre>
<p>When you run <code>git add</code>, you are telling git to track the changes made to a file. In this case, the changes are that there is a new file. <code>Git</code> now knows that it should pay attention to this file (or changes that have been made to the file), but the changes are <em>not</em> yet saved. Changes that have been added are also called changes that have been “staged”. There are many steps to this version control business.</p>
</div>
<div id="save-a-version-with-git-commit" class="section level1">
<h1>Save a version with <code>git commit</code></h1>
<p>You save a version of your repository with the subcommand <code>commit</code>. This will open up an editor, and you will be prompted to write a message. For now, you can type the message “Added first data file”.</p>
<pre class="bash"><code>$ git commit</code></pre>
<pre class="bash"><code>$ git commit -m &quot;Added first data file&quot;</code></pre>
<p>When you commit changes, you create a snapshot of all of the tracked files, a snapshot that you will be able to access at a later date. The point of a commit message is to give those changes an explicit context, context to help explain why you’ve made changes. Ideally, commit messages will be informative, describing some specific goal that the changes have accomplished (see the above link for more details). There is an art to writing good commit messages (<a href="https://chris.beams.io/posts/git-commit/">for a software engineer’s take, see here</a>). But not every commit message needs to be poetry. For example, when you’re done working for the day, the project might not have a particularly clear context, and yet you will still need to backup your code. Plenty of messages look like “stopped for the day”.</p>
</div>
<div id="review-the-history-with-git-log" class="section level1">
<h1>Review the history with <code>git log</code></h1>
<p>To see a list of the commits you’ve made, use <code>log</code>.</p>
<pre class="bash"><code>$ git log</code></pre>
<p>With each commit you make, <code>Git</code> associates a long ID (e.g., <code>f9390d95f5e369e12d6a65c8f5fa70b123cf8343</code>). You will not typically need to use that ID directly (more in the next session). The log also contains information about the author of the commit, their email (i.e., the things you setup with <code>git config</code>), when the commit was made, and what message was written about the commit.</p>
<p>For easier browsing, the logs can be condensed with the <code>--oneline</code> flag.</p>
<pre class="bash"><code>$ git log --oneline</code></pre>
<p>Each commit describes a project during a certain moment in time. As a researcher, you probably won’t care about most moments (e.g., you probably won’t ever need to go back to the status of the code at the end of each day). It’s good that they are backed up, but you won’t ever need to return to those versions.</p>
<p>You now have a system for backing up versions indefinitely. Rather than having version</p>
<p>With <code>Git</code>, as</p>
</div>
<div id="mark-important-commits-with-git-tag" class="section level1">
<h1>Mark important commits with <code>git tag</code></h1>
<p>To mark a particular commit as important, you “tag” it. First, move the file “fish_figure.R” into your repository, stage it, then commit it. I’ve written the message “Added analyses of data”.</p>
<pre class="bash"><code>$ git add fish_figure.R
+ git commit</code></pre>
<p>You will now use a new subcommand to mark this as an important commit, <code>tag</code>. Just like when you are committing something, you are given the opportunity to add a bit of context to the tag, and why it might be important. If this is the stage of the code right before a meeting with my adviser, my tag message might look like “before meeting; I’m going to talk about some cool plots”</p>
<pre class="bash"><code>$ git tag -a oct-1-2020</code></pre>
<p><code>-a</code> stands for “annotate”. The annotation is given by “oct-1-2020”. The annotation will be a method for finding that commit. I tag many projects before each meeting with an adviser, so many of my tags are dates.</p>
</div>
<div id="to-view-previous-versions-of-the-code-use-git-checkout" class="section level1">
<h1>To view previous versions of the code, use <code>git checkout</code></h1>
<p>You’ve made two commits, and indicated that the second one is important. Now, you will see how to look at previous versions of your code.</p>
<p>First, make some small change to <code>fish_figure.R</code>. For example, you might add the following comment to the top of the script <code># Here is a new comment at the top of the script.</code> After making the change, track those changes, and then commit. This time, write the commit message a bit differently.</p>
<pre class="bash"><code>$ git add fish_figure.R
+ git commit -m &quot;Add line comment&quot;</code></pre>
<p>Notice that the commit message is written slightly differently. Adding a single comment to <code>fish_figure.R</code> was very minor, and so there is not a lot of context. By passing <code>-m</code>, you can also type the message directly, without needing to open up the text editor.</p>
<p>Take a look at the logs to see the current version in relation to previous commits</p>
<pre class="bash"><code>$ git log --oneline</code></pre>
<p>At this point, there are a total of 3 commits, the second one of which you’ve tagged. To visit the project at that moment in time, you use <code>git checkout</code>, and you pass it the annotation you provided earlier.</p>
<pre class="bash"><code>$ git checkout oct-1-2020</code></pre>
<p>You can ignore most of the output on the console. But do go look at the file <code>fish_figure.R</code>. You will see that the comment is not there. That is, you’ve visited the files exactly as they looked when you made a tag.</p>
<p>To return to the current state of the files, run the following</p>
<pre class="bash"><code>$ git checkout master</code></pre>
<p>Confirm that the third change you committed is in the current working directory (e.g., the comment that you added should be back in the file).</p>
<div id="side-note" class="section level2">
<h2>Side note</h2>
<p>You can acess every commit based on the automatically generated ID. To see all of the commits and their IDs, again use the log command.</p>
<pre class="bash"><code>$ git log</code></pre>
<p>Your IDs will be different. But if I wanted to get to the commit tagged “oct-1-2020”, I could also type the following</p>
<pre class="bash"><code>$ git checkout dc160a659f7eb6503c78b3e28b659a91e7d0d89b</code></pre>
<p>Notice also that reference to ‘master’ again, which has not yet been explained. More on that next.</p>
</div>
</div>
<div id="interim" class="section level1">
<h1>Interim</h1>
<p>At this point, you’ve started tracking files in a repository, and you’ve explicitly written out the context of all of those changes. The following is a bit esoteric, but I’m mentioning it as a way to start an explanation for what “branch” and “master” refers to, and to prepare you as you look at more resources.</p>
<p>One way to view a “repository” is as a graph, in the <a href="https://en.wikipedia.org/wiki/Graph_theory">graph theory sense</a>, with nodes and edges between those nodes.</p>
<p>As you continually commit changes, you will add nodes to this graph, one node for each commit. Each node will have an edge, pointing some earlier commit. Some of those nodes will be easily (the tagged ones), but all are accessible.</p>
<p>Notice also that this graph is one long chain, with each commit pointing to exactly one unique node. There are ways to add ‘branches’ to the graph, where you can maintain separate versions of your code simultaneously. With this branching, <code>Git</code> becomes very powerful. The primary branch is often called the “master” branch. This is currently the default name on a new <code>Git</code> repository (but see below). Similarly to how you can pass both IDs and a tag’s annotation to <code>git checkout</code>, you can also pass the name of a branch. When you use the name of a branch, <code>Git</code> checks out the most recent commit on that branch. So, earlier, when you ran <code>git checkout master</code>, it’s like you told <code>Git</code> to “convert all files to the most recent commit of the master branch”, which happened to be the most recent versions of the files. Branching gets complicated very quickly. For more information, see the references at the end of this document (especially <a href="https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell">the branching chapter</a> of the Git Pro book). As you learn more about <code>Git</code>, branches may become important, and you will continue to encounter this graph metaphor.</p>
</div>
<div id="extra" class="section level1">
<h1>Extra</h1>
<div id="gitignore" class="section level2">
<h2>.gitignore</h2>
<p>Sometimes, you will want to explicitly ignore files in the folder. These can be done by adding those files to a <code>.gitignore</code> file. We talked about how a <code>.gitignore</code> folder might contain the line <code>hippa</code>, if you wanted to always make sure that <code>Git</code> ignored a folder called “hippa”.</p>
<p>Most coding languages generate temporary files that you don’t need to store. For this reasons, GitHub has a repository dedicated to commonly used <code>.gitignore</code> files, <a href="https://github.com/github/gitignore">here</a>. For example, <a href="https://github.com/github/gitignore/blob/master/R.gitignore">here is a file for R</a>, and <a href="https://github.com/github/gitignore/blob/master/Python.gitignore">here is one for python</a>. You could add the lines of those files to the <code>.gitignore</code> file in your own projects. See how easy it is to share code with GitHub :).</p>
</div>
<div id="larger-files" class="section level2">
<h2>Larger Files</h2>
<p><code>Git</code> is best at storing code, files that can be opened in a text editor. If you start storing larger files, binaries, things that end in ‘.pdf’, ‘.docx’, ‘.png’, etc, your repository will quickly become very large, and the different commands will start taking a long time to run. For this reason, you will often want to avoid adding such files to your repository (they can be inside your folder, but don’t call <code>git add</code> on them). There are two points here. First, <code>Git</code> isn’t for everything. For example, I organize my pdfs with Zotero, and backup those pdfs using box. Second, there are other tools for version control that integrate very smoothly with <code>Git</code>. I like <a href="https://git-lfs.github.com/"><code>Git LFS</code></a> (Large File Storage). For example, when an experiment involves images, I track the experimental code with <code>Git</code> and the images with <code>Git LFS</code>. If all of the images are stored as .png files in the repository folder, typing the following will put those under version control by <code>Git LFS</code>.</p>
<p>This allows me to keep a <code>Git</code>-focused workflow, while still allowing the images to be connected with the experiment.</p>
<p>The main competitor to LFS is <a href="https://git-annex.branchable.com/"><code>Git-annex</code></a>. Git annex is much harder to use, and, as compared to LFS, Git-annex doesn’t play nicely with GitHub/GitLab. However, the fMRI community has built a tool on top of <code>Git-annex</code>, called <a href="https://www.datalad.org/">datalad</a>. <code>datalad</code> promises to bring collaborative version control to neuroimaging.</p>
</div>
</div>
